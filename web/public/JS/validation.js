function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

$("#pwd,#pwd1").keyup(function () {

    var val = $(this).val();
    if (val.length < 10 || !(val.match(/[A-z]/)) || !(val.match(/[A-Z]/)) || !(val.match(/[0-9]/))) {
        $(this).css({ borderColor: 'red', color: 'red' });
    }
    else {
        $(this).css({ borderColor: 'green', color: 'green' });
    }
});

$("#btnCreerCompte").on('click', function (e) {
    // e.preventDefault();
    var pwd, pwd1, mail;
    pwd = $("#Contactpwd1").val();
    pwd1 = $("#Contactpwd2").val();
    mail = $("#ContactEmail").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#mailHelpBlockMD1").removeClass("invisible");

    }
    if (pwd.length < 8 || pwd.length > 20 || !(pwd.match(/[A-z]/)) || !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/))) {

        e.preventDefault();
        $("#passwordHelpBlock1").removeClass("invisible");
    }
    if (pwd !=  pwd1 ){

        e.preventDefault();
        $("#passwordHelpBlock2").removeClass("invisible");
    }



});
$("#validConnexion").on('click', function (e) {
    // e.preventDefault();
    var pwd, pwd1, mail;
    pwd = $("#orangeForm-pass").val();
    mail = $("#orangeForm-email").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#mailHelpBlockMD").removeClass("invisible");

    }
    if (pwd.length < 8 || pwd.length > 20 || !(pwd.match(/[A-z]/)) || !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/))) {

        e.preventDefault();
        $("#passwordHelpBlockMD").removeClass("invisible");
    }


});
$("#validRecupPwd").on('click', function (e) {
    // e.preventDefault();
    var  mail;
   
    mail = $("#validRecupEmail").val();
    nom = $("#valideRecupNom").val();
    prenom = $("#valideRecupPrenom").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#recupPwdHelpBlockMD").removeClass("invisible");

    }
    if (nom.length < 3) {

        e.preventDefault();
        $("#recupNomHelpBlockMD").removeClass("invisible");

    }
    if (nom.length < 3) {

        e.preventDefault();
        $("#recupPrenomHelpBlockMD").removeClass("invisible");

    }
    
  

});