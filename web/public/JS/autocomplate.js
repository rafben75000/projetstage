$( "#zone_recherche" ).autocomplete({
  source: function( request, response ) {
   // Fetch data
   $.ajax({
    url: "./search-article",
    type: 'POST',
    dataType: "json",
    data: {
     'q': request.term
    },
    success: function( data ) {
     response( data );
    }
   });
  },
  select: function (event, ui) {
   $('#zone_recherche').val(ui.item.label); 
   return false;
  }
 });
 $( "#zone_recherche" ).focus(function(){
       
  $(this).css({width : '600px'});
  
});
$( "#zone_recherche" ).blur(function(){
  $(this).css({width : '300px'});
  
});
$(document).keydown(function(e){

       
  if (e.which == 73 && e.ctrlKey)
  $( "#zone_recherche" ).focus();


});