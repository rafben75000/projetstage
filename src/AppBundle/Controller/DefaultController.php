<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Utilisateurs;
use AppBundle\Entity\Contact;
use Symfony\Component\HttpFoundation\Session\Session;



class DefaultController extends Controller
{
    /**
     * @Route("/test1", name="test1epage")
     */
    public function test1Action()
    {
       
       return $this->render('default/test1.html.twig',[] );
    }
    /**
     * @Route("/tt", name="testepage")
     */
    public function testeAction(\Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Hello Email'))
        ->setFrom('imed.zina1308@gmail.com')
        ->setTo('imed_cpp2@yahoo.fr')
        ->setBody('Mail de teste') ;
        $mailer->send($message);

       return $this->render('default/test.html.twig',[] );
    }
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        $session=new Session();
        //$em = $this->getDoctrine()->getManager();        
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_theme("Mathematiques");
        $resultsChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_theme("Physique Chimie");
        $resultsBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_theme("Biologie");
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles"); 
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");       
               
              
        
        return $this->render('default/index.html.twig', ['carousel'=>$results, 'math'=>  $resultsMath, 'chimie' => $resultsChimie, 'biologie'=>$resultsBiologie, 
        'sousmath'=>$resultsSousThemMath,'sousbio'=>$resultsSousThemBiologie, 
        'souschimie'=>$resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO ] );
    }
       
    /**
     * @Route("/contact", name="contactpage")
     */
    public function contactAction(Request $request)
    {
        // replace this example code with whatever you need
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");  

        ///////////////////////////////////
        if ($request->getMethod() == 'POST') {
            $nom = $request->get('nom');
            $mail = $request->get('mail');
            $sujet = $request->get('sujet');
            $message = $request->get('message');
            $contact = new Contact();
            $contact->setNom($nom);
            $contact->setSujet($sujet);
            $contact->setMail($mail);
            $contact->setMessage($message);
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $session = new Session();
            $session->getFlashBag()->add("info", "Bonjour, " . $nom . " Votre demande est enregistrée");
        }


        //////////////////////////////

        return $this->render('default/contact.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath,'sousbio'=>$resultsSousThemBiologie, 
        'souschimie'=>$resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO ]);
    }
    /**
     * @Route("/recherche", name="recherchepage")
     */
    public function rechercheAction(Request $request)
    {
        // replace this example code with whatever you need
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();

        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");  
        
        return $this->render('default/rechercheavancee.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath, 
        'sousbio'=>$resultsSousThemBiologie, 'souschimie'=>$resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO ]);
    }
    /**
     * @Route("/presentation", name="presentationpage")
     */
    public function presentationAction(Request $request)
    {
        // replace this example code with whatever you need
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();

        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");  

        return $this->render('default/presentation.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath, 'sousbio'=>$resultsSousThemBiologie, 
        'souschimie'=>$resultsSousThemChimie,'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO ]);
    }
    /**
     * @Route("/recupemdp", name="recupmdppage")
     */
    public function recupmdpAction(Request $request,\Swift_Mailer $mailer)
    {
        // replace this example code with whatever you need
        $session = new Session();
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");
        if ($request->getMethod() == 'POST') {
            $nom = $request->get('nom');
            $prenom = $request->get('prenom');
            $mail = $request->get('mail');
            $donnes = $this->getDoctrine()->getRepository('AppBundle:Utilisateurs');
            $utilisateur = $donnes->findOneBy(["mail" => $mail]);
            if(!$utilisateur)
                $session->getFlashBag()->add("infomail", "Bonjour, Votre compte n'existe pas.");
             else {
                 $session->getFlashBag()->add("infomail", "Votre mot de passe vous a été envoyé, veuillez consulter votre boite mail");
                $message = (new \Swift_Message('Recupération du mot de passe'))
                ->setFrom('n77.php@gmail.com')
                ->setTo('rbengrid@hotmail.com')
                ->setBody($this->renderView('default/mailRecup.html.twig',array('prenom' => $prenom, 'pwd'=>$utilisateur->getPwd())
                ),
                'text/html') ;
                $mailer->send($message);
               // dump($nom,$prenom,$mail);die();
        }
        }
        

        
        return $this->render('default/recuperationmdp.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath, 
        'sousbio' => $resultsSousThemBiologie, 'souschimie' => $resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
    
    /**
     * @Route("/creercompte", name="creercomptepage")
     */
    public function creercompteAction(Request $request)
    {
        // replace this example code with whatever you need
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $session = new Session();
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");
       
        /////////////////////////
    if ($request->getMethod() == 'POST') {
        $nom = $request->get('nom');
        $prenom = $request->get('prenom');
        $mail = $request->get('mail');
        $pwd = $request->get('pwd');
        $em = $this->getDoctrine()->getManager();
        $donnes = $this->getDoctrine()->getRepository('AppBundle:Utilisateurs');
        $utilisateur = $donnes->findOneBy(["mail" => $mail]);
        if (!$utilisateur) {
            $user = new Utilisateurs();
            $user->setNom($nom);
            $user->setPrenom($prenom);
            $user->setMail($mail);
            $user->setPwd($pwd);
            $em->persist($user);
            $em->flush();
            $session->getFlashBag()->add("infocreation", "Bonjour " . $nom . ", votre compte est crée.");
        } else {
            $session->getFlashBag()->add("infocreation", "Cette adresse mail existe déjà");
        }
    }
        ////////////////////////////////:
        
        
        return $this->render('default/creer_compte.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath, 
        'sousbio' => $resultsSousThemBiologie, 'souschimie' => $resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
      /**
     * @Route("/afficher/{id}", name="livre_show")
     */
    public function affiche_livreAction($id)
    {
        
        $em = $this->getDoctrine()->getManager();        
        $donnes = $this->getDoctrine()->getRepository('AppBundle:livres');
        $livre=$donnes->find($id);
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        // replace this example code with whatever you need
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");

        return $this->render('default/livreSelectionne.html.twig',['carousel'=>$results, 
        'livre'=>$livre, 'sousmath'=>$resultsSousThemMath, 'sousbio' => $resultsSousThemBiologie,
         'souschimie' => $resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
    /**
     * @Route("/search-article", name="search_article", defaults={"_format"="json"})
     * @Method("POST")
     */
    public function searchArticleAction(Request $request)
    {
            
       
        //$q = $request->query->get('term'); // use "term" instead of "q" for jquery-ui
        $q=$request->get('q');
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->findLike($q); 
        $results2 = $this->getDoctrine()->getRepository('AppBundle:livres')->findLike2($q);        
               
        return $this->render("default/autocomplatesearch.json.twig", ['articles' => $results,'auteurs'=>$results2]);
    }
      /**
     * @Route("/sousTheme/{sousTheme}", name="sousTheme_show")
     * @Method("GET")
     */
    public function sousThemeAction($sousTheme,Request $request)
    {
        
        $em = $this->getDoctrine()->getManager();
        $livreSousTheme = $this->getDoctrine()->getRepository('AppBundle:livres')->resultatSThem($sousTheme);  
        $dql   = "SELECT l FROM AppBundle:livres l where l.sousTheme LIKE '".$sousTheme."'";

        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
      
        $query = $em->createQuery($dql);
        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate( $query, $request->query->getInt('page', 1), 3);

        // replace this example code with whatever you need
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique");
 
        return $this->render('default/sousTheme.html.twig',['carousel'=>$results,
        'sousmath'=>$resultsSousThemMath,'sousbio'=>$resultsSousThemBiologie,
         'souschimie'=>$resultsSousThemChimie, 'sousTheme'=>$pagination, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
     /**
     * @Route("/resultatrecherche", name="resultatrecherche")
     * @Method("POST")
     */
    public function resultatrechercheAction(Request $req)
    {
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $titre=$req->get('autocomplatereche');        
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();

        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");        
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique"); 

        $resultsTitre = $this->getDoctrine()->getRepository('AppBundle:livres')->rechercheTitre($titre);      
        return $this->render('default/resultatrecherche.html.twig',['carousel'=>$results, 'sousmath'=>$resultsSousThemMath, 'sousbio' => $resultsSousThemBiologie,
         'souschimie' => $resultsSousThemChimie,'titretrouve'=>$resultsTitre, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
    /**
     * @Route("/resultatrechercheavancer", name="resultatrechercheavancer")
     * @Method("POST")
     */
    public function resultatRechercheAvanceAction(Request $req)
    {
        $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id);
        $titre=$req->get('titreTrouve');
        $auteur=$req->get('auteurTrouve');
        $EAN=$req->get('EANTrouve');
        $theme=$req->get('themeTrouve');        
        if($titre == "")
            $titre="******";
        if($auteur == "")
            $auteur="*****";
        
        $resultsRechercheAvance = $this->getDoctrine()->getRepository('AppBundle:livres')->rechercheAvance($titre,$auteur,$EAN,$theme);
        //dump($titre,$resultsRechercheAvance);die();
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique"); 
        

        //dump($resultsTitre);die();
        
        //
       
        return $this->render('default/resultatrecherche.html.twig',['carousel'=>$results, 'sousmath'=>$resultsSousThemMath, 'sousbio' => $resultsSousThemBiologie,
         'souschimie' => $resultsSousThemChimie,'titretrouve'=>$resultsRechercheAvance, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,"backO"=>$BackO]);
    }
     /**
     * @Route("/connexion", name="Connexion")
     * @Method("POST")
     */
     public function ConexionAction(Request $request)
    {
        // replace this example code with whatever you need
        $session = new Session();
        $mail = $request->get('mail');
        $pwd = $request->get('pwd');
        $em = $this->getDoctrine()->getManager();
        $donnes = $this->getDoctrine()->getRepository('AppBundle:Utilisateurs');
        $utilisateur = $donnes->findBy(["mail" => $mail,"pwd"=>$pwd]);
        if($utilisateur){
            $this->get('session')->set('loginUser', "OKconnexion");
            $this->get('session')->set('loginUserID', $utilisateur[0]->getMail());            
            $session->getFlashBag()->add("info", "Bonjour, " . $utilisateur[0]->getNom());
            //dump($mail,$pwd);die();
        }else{
            $this->get('session')->set('loginUser', "NOconnexion"); 
            $session->getFlashBag()->add("info", "Echec de connexion, veuillez réessayer");                           
           // dump($mail);die();
        }
        return $this->redirectToRoute('homepage');   
       
    }
    /**
     * @Route("/dconnexion", name="Deconnexion")
     */
    public function DeconnexionAction(Request $request)
    {
        $this->get('session')->remove('loginUser');
        $this->get('session')->clear();        
        return $this->redirectToRoute('homepage');
    }
    /**
     * @Route("/downlod/{url}", name="Download")
     */
   
    public function telechargementCvPdfAction($url){
    
        $fichier = "\\".$url;
        $chemin = "uploads\livres" ;
        header ("Content-type: application/force-download");
        header ("Content-disposition: filename=$fichier");     
        readFile($chemin . $fichier);             
        $url = $this->generateUrl('nom_routing');
        return $this->redirect($url);
    }
    /**
     * @Route("/confmodification", name="confmodification")
     * @Method("POST")
     */
    public function confirmationModifCompteAction(Request $request)
    {
       
        if($this->get('session')->get('loginUser')==NULL)
            return $this->redirectToRoute('homepage');
        if ($request->getMethod() == 'POST') {
            $session = new Session();
            $mail=$this->get('session')->get('loginUserID');
            $entityManager = $this->getDoctrine()->getManager();
            $utilisateur = $entityManager->getRepository('AppBundle:Utilisateurs')->findOneBy(["mail" => $mail]);
           // $donnes = $this->getDoctrine()->getRepository('AppBundle:Utilisateurs');
           // $utilisateur = $donnes->findOneBy();
            $nom = $request->get('nom');
            $prenom = $request->get('prenom');
            $pwd = $request->get('pwd');        
            $utilisateur->setNom($nom);
            $utilisateur->setPrenom($prenom);           
            $utilisateur->setPwd($pwd);
            $entityManager->flush();
            $session->getFlashBag()->add("info", " Votre modification est effectuée avec succès.");
            }
        return $this->redirectToRoute('modification'); 
       
    }

    /**
     * @Route("/modification", name="modification")
     */
    public function modifCompteAction(Request $request)
    {
        // replace this example code with whatever you need
        if($this->get('session')->get('loginUser')==NULL)
            return $this->redirectToRoute('homepage');   
            $donnesBackO = $this->getDoctrine()->getRepository('AppBundle:Backoffice');
        $id=1;
        $BackO=$donnesBackO->find($id); 
        $session = new Session();
        $results = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_carousel();
        $resultsSousThemMath = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Mathematiques");
        $resultsSousThemBiologie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Biologie");
        $resultsSousThemChimie = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Physique Chimie");
        $resultsSousIndustrielles = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Sciences Industrielles");
        $resultsSousLettre = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Lettre");
        $resultsSousInformatique = $this->getDoctrine()->getRepository('AppBundle:livres')->resultat_sous_theme("Informatique"); 
        $mail=$this->get('session')->get('loginUserID');
        $donnes = $this->getDoctrine()->getRepository('AppBundle:Utilisateurs');
        $utilisateur = $donnes->findOneBy(["mail" => $mail]);
        return $this->render('default/modification.html.twig',['carousel'=>$results,'sousmath'=>$resultsSousThemMath, 
        'sousbio' => $resultsSousThemBiologie, 'souschimie' => $resultsSousThemChimie, 'sousindustrielles'=>$resultsSousIndustrielles,'souslettre'=> $resultsSousLettre,'sousInformatique'=>$resultsSousInformatique,
        'utilisateur'=>$utilisateur,"backO"=>$BackO]);
    }


    
}
