function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test(email);
}

$("#pwd,#pwd1").keyup(function () {

    var val = $(this).val();
    if (val.length < 10 || !(val.match(/[A-z]/)) || !(val.match(/[A-Z]/)) || !(val.match(/[0-9]/))) {
        $(this).css({ borderColor: 'red', color: 'red' });
    }
    else {
        $(this).css({ borderColor: 'green', color: 'green' });
    }
});

$("#btnCreerCompte").on('click', function (e) {
    // e.preventDefault();
    var pwd, pwd1, mail;
    pwd = $("#Contactpwd1").val();
    pwd1 = $("#Contactpwd2").val();
    mail = $("#ContactEmail").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#mailHelpBlockMD1").removeClass("invisible");

    }
    if (pwd.length < 8 || pwd.length > 20 || !(pwd.match(/[A-z]/)) || !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/))) {

        e.preventDefault();
        $("#passwordHelpBlock1").removeClass("invisible");
    }
    if (pwd !=  pwd1 ){

        e.preventDefault();
        $("#passwordHelpBlock2").removeClass("invisible");
    }



});
$("#validConnexion").on('click', function (e) {
    // e.preventDefault();
    var pwd, pwd1, mail;
    pwd = $("#orangeForm-pass").val();
    mail = $("#orangeForm-email").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#mailHelpBlockMD").removeClass("invisible");

    }
    if (pwd.length < 8 || pwd.length > 20 || !(pwd.match(/[A-z]/)) || !(pwd.match(/[A-Z]/)) || !(pwd.match(/[0-9]/))) {

        e.preventDefault();
        $("#passwordHelpBlockMD").removeClass("invisible");
    }


});
$("#submitContact").on('click', function (e) {
    // e.preventDefault();
    var  mail;
   
    mail = $("#FormContactMail").val();
    nom = $("#FormContactName").val();
    message = $("#FormContactMessage").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#mailHelpBlockMD1").removeClass("invisible");

    }
    if (nom.length < 3) {
        e.preventDefault();
        $("#nomHelpBlockMD1").removeClass("invisible");

    }
    if (message.length < 50) {

        e.preventDefault();
        $("#messageHelpBlockMD1").removeClass("invisible");

    }
});

$("#validRecupPwd").on('click', function (e) {
    // e.preventDefault();
    var  mail;
   
    mail = $("#validRecupEmail").val();
    nom = $("#valideRecupNom").val();
    prenom = $("#valideRecupPrenom").val();

    if (!validateEmail(mail) || mail == "") {

        e.preventDefault();
        $("#recupPwdHelpBlockMD").removeClass("invisible");

    }
    if (nom.length < 3) {

        e.preventDefault();
        $("#recupNomHelpBlockMD").removeClass("invisible");

    }
    if (nom.length < 3) {

        e.preventDefault();
        $("#recupPrenomHelpBlockMD").removeClass("invisible");

    }
});
$(".nav-item").on('click', function (e) {
    $( "li" ).each(function(){
        $(this).removeClass("active");
    });
    $(this).addClass("active");    
});
$('#btnmodal').trigger('click');
$('#btnmoalcompt').trigger('click');
// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function () { scrollFunction() };

function scrollFunction() {
    if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
}
$(".element-survole").hover(function(){
    $(this).attr('type','text'); 
});

$(".element-survole").mouseleave(function(){
    $(this).attr('type','password'); 
});

 