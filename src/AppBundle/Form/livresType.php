<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;





class livresType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('titre')->add('auteur')->add('description')->add('editeur')->add('eAN')->add('urlPhoto')->add('nbPages')
        ->add('date')
        ->add('theme', ChoiceType::class,array('choices'=>array('Mathématiques'=>'Mathematiques','Lettres'=>'Lettre','Biologie'=>'Biologie','Informatique'=>'Informatique', 'Physique Chimie'=>'Physique Chimie')))->add('sousTheme')->add('nouveaute')
        ->add('urlPieceJointe', FileType::class);
   
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\livres'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_livres';
    }


}
