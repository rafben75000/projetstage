<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * livres
 *
 * @ORM\Table(name="livres")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\livresRepository")
 */
class livres
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="auteur", type="string", length=255)
     */
    private $auteur;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="editeur", type="string", length=255)
     */
    private $editeur;

    /**
     * @var float
     *
     * @ORM\Column(name="EAN", type="float")
     */
    private $eAN;

    /**
     * @var string
     *
     * @ORM\Column(name="url_photo", type="string", length=255)
     */
    private $urlPhoto;

    /**
     * @var int
     *
     * @ORM\Column(name="nb_pages", type="integer")
     */
    private $nbPages;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=255)
     */
    private $theme;

    /**
     * @var string
     *
     * @ORM\Column(name="sous_theme", type="string", length=255)
     */
    private $sousTheme;

    /**
     * @var bool
     *
     * @ORM\Column(name="nouveaute", type="boolean")
     */
    private $nouveaute;

    /**
     * @var string
     *
     * @ORM\Column(name="url_piece_jointe", type="string", length=255)
     */
    private $urlPieceJointe;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return livres
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set auteur
     *
     * @param string $auteur
     *
     * @return livres
     */
    public function setAuteur($auteur)
    {
        $this->auteur = $auteur;

        return $this;
    }

    /**
     * Get auteur
     *
     * @return string
     */
    public function getAuteur()
    {
        return $this->auteur;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return livres
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set editeur
     *
     * @param string $editeur
     *
     * @return livres
     */
    public function setEditeur($editeur)
    {
        $this->editeur = $editeur;

        return $this;
    }

    /**
     * Get editeur
     *
     * @return string
     */
    public function getEditeur()
    {
        return $this->editeur;
    }

    /**
     * Set eAN
     *
     * @param float $eAN
     *
     * @return livres
     */
    public function setEAN($eAN)
    {
        $this->eAN = $eAN;

        return $this;
    }

    /**
     * Get eAN
     *
     * @return float
     */
    public function getEAN()
    {
        return $this->eAN;
    }

    /**
     * Set urlPhoto
     *
     * @param string $urlPhoto
     *
     * @return livres
     */
    public function setUrlPhoto($urlPhoto)
    {
        $this->urlPhoto = $urlPhoto;

        return $this;
    }

    /**
     * Get urlPhoto
     *
     * @return string
     */
    public function getUrlPhoto()
    {
        return $this->urlPhoto;
    }

    /**
     * Set nbPages
     *
     * @param integer $nbPages
     *
     * @return livres
     */
    public function setNbPages($nbPages)
    {
        $this->nbPages = $nbPages;

        return $this;
    }

    /**
     * Get nbPages
     *
     * @return int
     */
    public function getNbPages()
    {
        return $this->nbPages;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return livres
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set theme
     *
     * @param string $theme
     *
     * @return livres
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;

        return $this;
    }

    /**
     * Get theme
     *
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Set sousTheme
     *
     * @param string $sousTheme
     *
     * @return livres
     */
    public function setSousTheme($sousTheme)
    {
        $this->sousTheme = $sousTheme;

        return $this;
    }

    /**
     * Get sousTheme
     *
     * @return string
     */
    public function getSousTheme()
    {
        return $this->sousTheme;
    }

    /**
     * Set nouveaute
     *
     * @param boolean $nouveaute
     *
     * @return livres
     */
    public function setNouveaute($nouveaute)
    {
        $this->nouveaute = $nouveaute;

        return $this;
    }

    /**
     * Get nouveaute
     *
     * @return bool
     */
    public function getNouveaute()
    {
        return $this->nouveaute;
    }

    /**
     * Set urlPieceJointe
     *
     * @param string $urlPieceJointe
     *
     * @return livres
     */
    public function setUrlPieceJointe($urlPieceJointe)
    {
        $this->urlPieceJointe = $urlPieceJointe;

        return $this;
    }

    /**
     * Get urlPieceJointe
     *
     * @return string
     */
    public function getUrlPieceJointe()
    {
        return $this->urlPieceJointe;
    }
}

